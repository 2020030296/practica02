function agregarAlumno() {
        const Matricula = document.getElementById('matricula').value;
        const Nombre = document.getElementById('nombre').value;
        const Dom = document.getElementById('Domicilio').value;
        const sexo = document.getElementById('sexo').value;
        const esp = document.getElementById('Especialidad').value;
        if(Matricula){
            const alumno = {
                matricula : Matricula,
                nombre : Nombre,
                domicilio : Dom, 
                sexo : sexo,
                especialidad : esp
            }
            
            axios.post('/insertar', alumno)
            .then((response) => {
                console.log(response.data);
                const res = document.getElementById('tabla');
                res.innerHTML = "<tr><th>ID</th><th>Matricula</th><th>Nombre</th><th>Domicilio</th><th>Sexo</th><th>Especialidad</th></tr>"
                mostrarT();
            })
            .catch((error) => {
                console.log(error);
                const respuesta = document.getElementById('res');
                respuesta.innerHTML = '';
                respuesta.innerHTML = 'La matricula ya existe'
            });
        }
      }
     function mostrarT(){
        axios.get('/')
        .then((res)=>{
        mostrar(res.data);
        })
        .catch((error)=>{
            console.log("Surgio un error" + error);
        })
        function mostrar(data){
            const res = document.getElementById('tabla');
    
                for(item of data){
                        res.innerHTML += '<tr> <td>' + item.id + '</td> <td>' + item.matricula + '</td> <td>' + item.nombre + '</td><td>' + item.domicilio + 
                        '</td> <td>' + item.sexo + '</td> <td>' + item.especialidad + '</td> </tr>';
                }
        }
      }
      function buscar(){
        const Matricula = document.getElementById('matricula').value;
        if(Matricula){
            axios.get('/buscar', {params: {matricula: Matricula}})
            .then((res)=>{
                mostrar(res.data);
            })
            .catch((error)=>{
                console.log(error);
                const respuesta = document.getElementById('res');
                respuesta.innerHTML = '';
                respuesta.innerHTML = 'La matricula no esta registrada'
            })
            function mostrar(data){
                const res = document.getElementById('tabla');
                res.innerHTML = "<tr class='table-dark'><th>ID</th><th>Matricula</th><th>Nombre</th><th>Domicilio</th><th>Sexo</th><th>Especialidad</th></tr>"
    
                    for(item of data){
                            res.innerHTML += '<tr> <td>' + item.id + '</td> <td>' + item.matricula + '</td> <td>' + item.nombre + '</td><td>' + item.domicilio + 
                            '</td> <td>' + item.sexo + '</td> <td>' + item.especialidad + '</td> </tr>';
                    }
            }
        }
      }
      function eliminar(){
        const Matricula = document.getElementById('matricula').value;
        if(Matricula){
            axios.delete('/borrar', {params: {matricula:Matricula}})
            .then((response) => {
                console.log(response.data);
                const res = document.getElementById('tabla');
                res.innerHTML = "<tr class='table-dark'><th>ID</th><th>Matricula</th><th>Nombre</th><th>Domicilio</th><th>Sexo</th><th>Especialidad</th></tr>"
                mostrarT();
            })
            .catch((error) => {
                console.log(error);
                const respuesta = document.getElementById('res');
                respuesta.innerHTML = '';
                respuesta.innerHTML = 'La matricula no esta registrada'
            });
        }
    
      }
      function modificar(){
        const Matricula = document.getElementById('matricula').value;
        const Nombre = document.getElementById('nombre').value;
        const Dom = document.getElementById('Domicilio').value;
        const sexo = document.getElementById('sexo').value;
        const esp = document.getElementById('Especialidad').value;
        if(Matricula){
            const alumno = {
                nombre : Nombre,
                domicilio : Dom, 
                sexo : sexo,
                especialidad : esp,
                matricula : Matricula
            }
            axios.put('/actualizar', alumno)
            .then((response) => {
                console.log(response.data);
                const res = document.getElementById('tabla');
                res.innerHTML = "<tr class='table-dark'><th>ID</th><th>Matricula</th><th>Nombre</th><th>Domicilio</th><th>Sexo</th><th>Especialidad</th></tr>"
                mostrarT();
            })
            .catch((error) => {
                console.log(error);
                const respuesta = document.getElementById('res');
                respuesta.innerHTML = '';
                respuesta.innerHTML = 'La matricula no esta registrada'
            });
        }
      }
    
      function validar(){
        const Matricula = document.getElementById('matricula').value;
        const Nombre = document.getElementById('nombre').value;
        const Dom = document.getElementById('Domicilio').value;
        const sexo = document.getElementById('sexo').value;
        const esp = document.getElementById('Especialidad').value;
        const btnBuscar = document.getElementById('btnBuscar');
        const btnAgregar = document.getElementById('btnAgregar');
        const btnBorrar = document.getElementById('btnBorrar');
        const btnAc = document.getElementById('btnModificar');
    
        if(Matricula && Nombre && Dom && sexo && esp){
            btnBuscar.disabled=false;
            btnAgregar.disabled=false;
            btnAc.disabled=false;
            btnBorrar.disabled=false;
        }else if(Matricula){
            btnBuscar.disabled=false;
            btnBorrar.disabled=false;
        }else{
            btnBuscar.disabled=true;
            btnAgregar.disabled=true;
            btnAc.disabled=true;
            btnBorrar.disabled=true;
        }
      }